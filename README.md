MQTT Project by H.El Jjouaoui & H.Merini. <br />
-Using dht11 as a library for the Sensor.<br />
-Using Paho as a library to pub and sub messages using MQTT.<br />
<br />
Instructions:<br />
1-Run Capteur.py to launch the Sensor and publish the Humidity Value.<br />
2-Run Servo.py to init the Servo motor and subscribe to the Client.<br />
3-The servo motor will receive the right value to show the purcentage of humidty in the room.<br />
<br />
Thank you! 
