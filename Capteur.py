import RPi.GPIO as GPIO
import dht11
import time
import os
import paho.mqtt.client as mqtt

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
instance = dht11.DHT11(pin=16)#GPIO 16 pin 36

Server = 'mojito.homedruon.com'
client = mqtt.Client()
client.connect(Server)
client.loop_start()
while True:
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(16, GPIO.OUT)
        result = instance.read()
        if result.is_valid():
            Humidity = result.humidity
            print("Humidity: %d %%" % Humidity)
            if Humidity in range(0,40):
                Angle = 0
            if Humidity in range(40,60):
                Angle = 90 
            if Humidity in range(60,99):
                Angle = 180
            client.publish("Hamza/test",Angle)
client.loop_stop()
client.disconnect()



