import RPi.GPIO as GPIO
import time
import paho.mqtt.client as mqtt

Server = 'mojito.homedruon.com'

def angle_to_percent (angle) :
    if angle > 180 or angle < 0 :
        return False
    start = 4
    end = 12.5
    ratio = (end - start)/180 #Calcul ratio from angle to percent
    angle_as_percent = angle * ratio
    return start + angle_as_percent

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("Hamza/test")
    
def Read(client, userdata, msg):
    angle = float(msg.payload)
    print("Servo Motor's Angle : %d" %angle)
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)

    #Use pin 18 for PWM signal
    pwm_gpio = 18
    frequence = 50
    GPIO.setup(pwm_gpio, GPIO.OUT)
    pwm = GPIO.PWM(pwm_gpio, frequence)

    #Angle's Value
    pwm.start(angle_to_percent(angle))
    time.sleep(1)
    pwm.stop()
    #Close GPIO & cleanup
    GPIO.cleanup()

client = mqtt.Client()   
client.on_connect = on_connect
client.on_message = Read
client.connect(Server)
client.loop_forever()


